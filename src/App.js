import "./App.css";
import React, { useState } from "react";

function App() {
  let id = "id" + Math.random().toString(16).slice(2);
  const [fulldata, setfulldata] = useState([]);
  const [data, setdata] = useState({
    id: id,
    namabarang: "",
    quantity: 0,
    harga: 0,
  });

  // console.log(id);

  const handleChange = (e) => {
    e.persist();
    setdata((data) => ({
      ...data,
      [e.target.name]: [e.target.value],
    }));
  };
  // console.log(data)

  const handleSubmit = (e) => {
    e.preventDefault();

    // setfulldata((oldArray) => [...oldArray, data]);
    // setfulldata([data]);
    setfulldata((prevArray) => [...prevArray, data]);
    console.log(fulldata);
  };

  return (
    <div className="container mt-2">
      <div>
        <h4>Data</h4>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="formGroupExampleInput" className="form-label">
              Nama Barang
            </label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder="Nama Barang"
              name="namabarang"
              onChange={handleChange}
            />
            <label htmlFor="formGroupExampleInput" className="form-label">
              Quantity
            </label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder="Quantity"
              name="quantity"
              onChange={handleChange}
            />
            <label htmlFor="formGroupExampleInput" className="form-label">
              Harga
            </label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder="Harga"
              name="harga"
              onChange={handleChange}
            />

            <button type="submit" className="btn btn-primary mt-3">
              Primary
            </button>
          </div>
        </form>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Quantity</th>
            <th scope="col">Harga</th>
            <th scope="col">Total Bayar</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        <tbody>
          {/* TIPE PERULANGAN / LOOP NYA */}

          {fulldata.map((arrayData) => {
            return (
              <tr key={arrayData.id}>
                <th scope="row">{arrayData.id}</th>
                <td>{arrayData.namabarang}</td>
                <td>
                  {arrayData.quantity
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                </td>
                <td>
                  Rp.{" "}
                  {arrayData.harga
                    .toString()
                    .split("")
                    .reverse()
                    .join("")
                    .match(/\d{1,3}/g)
                    .join(".")
                    .split("")
                    .reverse()
                    .join("")}
                </td>
                <td>Rp. 
                  {(arrayData.harga * arrayData.quantity)
                    .toString()
                    .split("")
                    .reverse()
                    .join("")
                    .match(/\d{1,3}/g)
                    .join(".")
                    .split("")
                    .reverse()
                    .join("")}
                </td>
                <td>
                  <button type="button" className="btn btn-primary">
                    Primary
                  </button>
                  <button type="button" className="btn btn-warning ms-1">
                    Ubah
                  </button>
                  <button type="button" className="btn btn-danger ms-1">
                    Hapus
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
